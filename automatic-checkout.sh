#!/usr/bin/env bash

cd /home/pi/project/proyecyo-leela/;

git fetch;
LOCAL=$(git rev-parse HEAD);
REMOTE=$(git rev-parse @{u});

#if our local revision id doesn't match the remote, we will need to pull the changes
if [ $LOCAL != $REMOTE ]; then
    #pull and merge changes
    git pull origin master;

    cd sensor_luminocidad_serie_c;
    make clean;
    make build;
    sudo reboot;

fi
#include <linux/module.h>   /* Needed by all modules */
#include <linux/kernel.h>   /* Needed for KERN_INFO */
#include <linux/timer.h>
#include <linux/moduleparam.h>
MODULE_LICENSE("GPL");

//	Insertar modulo : sudo insmod timer.ko frecuencia_parpadeo=13 frecuencia_actualizacion=30

static struct timer_list my_timer;
static int counter;
static int frecuencia_parpadeo;
module_param(frecuencia_parpadeo, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
MODULE_PARM_DESC(frecuencia_parpadeo, "frecuencia de parpadeo de parpadeo en segundos");

static int frecuencia_actualizacion;
module_param(frecuencia_actualizacion, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
MODULE_PARM_DESC(frecuencia_actualizacion, "frecuencia de actualizacion en segundos");

//extern int asm_main(void);
void my_timer_callback( unsigned long data )
{
	counter++;
	//counter=asm_main();
	printk(KERN_INFO "Parpadear cada = %d\n",frecuencia_parpadeo);
	mod_timer(&my_timer, jiffies + msecs_to_jiffies(frecuencia_actualizacion*1000));
}
int init_module(void)
{
	counter=0;
	printk(KERN_INFO "Insertando modulo timer...\n");
	printk(KERN_INFO "Parpadear cada = %d\n",frecuencia_parpadeo);
	setup_timer(&my_timer, my_timer_callback, 0);
	/* setup timer interval to 200 msecs */
	mod_timer(&my_timer, jiffies + msecs_to_jiffies(frecuencia_actualizacion*1000));
	return 0;
}

void cleanup_module(void)
{
	printk(KERN_INFO "Removiendo modulo timer...\n");
	del_timer(&my_timer);
}

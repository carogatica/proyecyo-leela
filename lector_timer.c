#include <stdio.h>
#include <stdlib.h>

// as -o asm_random.o asm_random.S
// gcc -o timer timer.c asm_random.o

int obtener_frecuencia(void);
int estado_timer(void);
int obtener_frecuencia_kernel(void);
extern int asm_main(void);

int main(int argc, char **argv) {
    printf("Estado del modulo timer = %d\n",estado_timer());
    printf("Frecuencia de kernel    = %d\n",obtener_frecuencia_kernel());
    printf("Frecuencia aleatoria    = %d\n",obtener_frecuencia());
    int random_number = asm_main();
    printf("Random = %d\n",random_number );
    return 0;
}
int estado_timer(){
    char buff[BUFSIZ];
    FILE *fp = popen("lsmod | awk '{print $1}' | grep ^timer$ | wc -l", "r");
    fgets( buff, BUFSIZ, fp );
    pclose(fp);
    if (atoi(buff)){
        return 1;
    }
    else{
        return 0;
    }
}
int obtener_frecuencia(){
    int frecuencia_kernel= obtener_frecuencia_kernel();
    int random = asm_main()%5;
    int frecuencia_aleatoria=frecuencia_kernel+random-2;
    return frecuencia_aleatoria;
}

int obtener_frecuencia_kernel(){
    char buff[BUFSIZ];
    FILE *fp = popen(" cat /var/log/syslog | grep -a 'Parpadear' |tail -n 1 | awk -F '=' '{print $2 }'","r");
    fgets( buff, BUFSIZ, fp );
    pclose(fp);
    return atoi(buff);
}